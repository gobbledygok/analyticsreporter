package com.metrics.facebook.service;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.facebook.ads.sdk.APIException;

@Controller
public class FacebookController {

	private static final Logger logger = Logger.getLogger(FacebookController.class);

	@RequestMapping("/getreportfromfacebook")
	public String getReport() {
		String response = "";

		try {
			CreateAdReports.createAddReport();
		} catch (APIException e) {
			logger.error(e);
		}

		return response;
	}

}
