package com.metrics.facebook.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.facebook.ads.sdk.APIContext;
import com.facebook.ads.sdk.APIException;
import com.facebook.ads.sdk.APINodeList;
import com.facebook.ads.sdk.AdAccount;
import com.facebook.ads.sdk.AdsInsights;
import com.facebook.ads.sdk.AdsInsights.EnumBreakdowns;

/**
 * Copyright (c) 2015-present, Facebook, Inc. All rights reserved.
 *
 * You are hereby granted a non-exclusive, worldwide, royalty-free license to
 * use, copy, modify, and distribute this software in source code or binary form
 * for use in connection with the web services and APIs provided by Facebook.
 *
 * As with any software that integrates with the Facebook platform, your use of
 * this software is subject to the Facebook Developer Principles and Policies
 * [http://developers.facebook.com/policy/]. This copyright notice shall be
 * included in all copies or substantial portions of the software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

public class CreateAdReports {

	private static final Logger logger = Logger.getLogger(CreateAdReports.class);

	public static void createAddReport() throws APIException {

		String access_token = "EAAB1oF4g7wgBAH3xSB9k8r7FgcSsiZC1LVJS2ySCwybPfBw3ZA3TW5ZAZBJJDlcO38ARmwAZBjgaWIVs5UKHkAp9K6uPG6pdZAX3biqI0xDkJZBmSXIYLnXhMvbHJWBaDdsbqDuT3XKkQ6SP7fX594ZBD7FhJebkX3z2hZAGBmj1OzdYeZAVuRqZAQGsTSzzsxDGjkhk7bOPMI8KfNlRCG2imJW";
		String ad_account_id = "116919482413453";
		String app_secret = "5ef41f4212f1c1408acc0b7dc3b433c9";
		String app_id = "129331634433800";
		APIContext context = new APIContext(access_token).enableDebug(true);

		List<EnumBreakdowns> enums = new ArrayList<EnumBreakdowns>();
		for (EnumBreakdowns e : EnumBreakdowns.values()) {
			enums.add(e);
		}

		APINodeList<AdsInsights> nodeList = new AdAccount(ad_account_id, context).getInsights()
				.setLevel(AdsInsights.EnumLevel.VALUE_ACCOUNT).setFiltering("[]").setBreakdowns(enums)
				.setTimeRange("{\"since\":\"2017-10-10\",\"until\":\"2017-11-09\"}").requestField("results")
				.requestField("result_rate").requestField("reach").requestField("frequency").requestField("impressions")
				.requestField("delivery").requestField("social_reach").requestField("social_impressions")
				.requestField("total_actions").requestField("total_unique_actions")
				.requestField("relevance_score:score").requestField("relevance_score:positive_feedback")
				.requestField("relevance_score:negative_feedback").requestField("spend").requestField("today_spend")
				.requestField("impressions_gross").requestField("impressions_auto_refresh")
				.requestField("cost_per_result").requestField("cpp").requestField("cpm")
				.requestField("cost_per_total_action").execute();

		logger.info("End of create ad report");

	}
}
