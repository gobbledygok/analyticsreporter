package com.metrics.criteo;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.rmi.RemoteException;
import java.util.Date;

import org.apache.axis2.AxisFault;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.AggregationType;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.ApiHeader;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.ApiHeaderE;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.ArrayOfCampaignStatus;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.ArrayOfInt;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.ArrayOfReportColumn;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.Campaign;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.CampaignSelectors;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.CampaignStatus;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.ClientLogin;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.ClientLoginResponse;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.GetCampaigns;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.GetCampaignsResponse;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.GetReportDownloadUrl;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.GetReportDownloadUrlResponse;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.ReportColumn;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.ReportJob;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.ReportSelector;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.ReportType;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.ScheduleReportJob;
import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.ScheduleReportJobResponse;
import com.metrics.util.DateUtil;

@Controller
public class CriteoController {

	private static final Logger logger = Logger.getLogger(CriteoController.class);

	public String endpoint = "https://advertising.criteo.com/API/v201305/AdvertiserService.asmx";
	public String source = "flotr_api2";
	public String user2 = "floadw@gmail.com";
	public String username = "alperboyer26@gmail.com";
	public String password = "FloZiylan2011!*";
	public Long appToken = new Long("2258648601625114624");
	public String address = "flotr_api2/apicriteo@criteo.com";

	@RequestMapping("/getreportfromcriteo")
	public String getReport() {

		CriteoAdvertiserAPIv201305Stub servicesStub = null;
		try {
			servicesStub = new CriteoAdvertiserAPIv201305Stub();
		} catch (AxisFault e2) {
			logger.error("Error in creating CriteoAdvertiserAPIv201305Stub " + e2);
		}
		try {
			ApiHeaderE headerE = getAuthenticationHeader(servicesStub);
			GetCampaignsResponse campains = getCampaigns(servicesStub, headerE);
			ScheduleReportJobResponse scheduleReportJobResponse = scheduleJob(servicesStub, headerE, campains);

			GetReportDownloadUrl request = new GetReportDownloadUrl();
			request.setJobID(scheduleReportJobResponse.getJobResponse().getJobID());
			GetReportDownloadUrlResponse report = null;
			try {
				report = servicesStub.getReportDownloadUrl(request, headerE);

			} catch (RemoteException e) {
				logger.error("Error in caling endpoint " + e);
			}

			generateFileFromReport(report);

		} catch (Exception e) {
			logger.error("Error in caling endpoint " + e);
		}

		return "welcome";
	}

	private void generateFileFromReport(GetReportDownloadUrlResponse report)
			throws FileNotFoundException, IOException, UnsupportedEncodingException {
		if (report != null) {
			URL website;
			try {
				website = new URL(report.getJobURL());
				ReadableByteChannel rbc = null;
				try {
					rbc = Channels.newChannel(website.openStream());
				} catch (IOException e) {
					logger.error("Error in generateFileFromReport " + e);
				}

				FileOutputStream fos = new FileOutputStream("report.html");
				fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

			} catch (MalformedURLException e) {
				logger.error("Error in generateFileFromReport " + e);
			}

		}
	}

	private ScheduleReportJobResponse scheduleJob(CriteoAdvertiserAPIv201305Stub servicesStub, ApiHeaderE headerE,
			GetCampaignsResponse campaigns) {
		ScheduleReportJob job = new ScheduleReportJob();
		ReportJob reportJob = new ReportJob();
		reportJob.setReportType(ReportType.Category);

		// set the column will return data for
		ArrayOfReportColumn column = new ArrayOfReportColumn();
		column.addReportColumn(ReportColumn.cost);
		column.addReportColumn(ReportColumn.impressions);
		column.addReportColumn(ReportColumn.revcpc);
		column.addReportColumn(ReportColumn.convRate);
		column.addReportColumn(ReportColumn.clicks);
		reportJob.setSelectedColumns(column);

		reportJob.setAggregationType(AggregationType.Daily);
		reportJob.setIsResultGzipped(false);

		ReportSelector selector = new ReportSelector();
		selector.setBannerIDs(null);
		ArrayOfInt campaignArray = getCampaigns(campaigns);
		selector.setCampaignIDs(campaignArray);
		selector.setCategoryIDs(null);
		reportJob.setReportSelector(selector);

		Date now = new Date();
		Date yesterday = DateUtil.yesterday();
		reportJob.setStartDate(DateUtil.convertDateToString(yesterday));
		reportJob.setEndDate(DateUtil.convertDateToString(now));

		job.setReportJob(reportJob);

		ScheduleReportJobResponse scheduleReportJobResponse = null;
		try {
			scheduleReportJobResponse = servicesStub.scheduleReportJob(job, headerE);
		} catch (RemoteException e1) {
			logger.error("Error in client login " + e1);

		}
		return scheduleReportJobResponse;
	}

	private ArrayOfInt getCampaigns(GetCampaignsResponse campaigns) {
		ArrayOfInt campaignArray = new ArrayOfInt();
		if (campaigns != null && campaigns.getGetCampaignsResult() != null
				&& campaigns.getGetCampaignsResult().getCampaign() != null) {
			int[] arr = new int[campaigns.getGetCampaignsResult().getCampaign().length];
			int i = 0;
			for (Campaign camp : campaigns.getGetCampaignsResult().getCampaign()) {
				if (camp != null)
					arr[i] = camp.getCampaignID();
				i++;
			}
			campaignArray.set_int(arr);

		}
		return campaignArray;
	}

	private GetCampaignsResponse getCampaigns(CriteoAdvertiserAPIv201305Stub servicesStub, ApiHeaderE headerE) {
		GetCampaignsResponse campaigns = null;
		GetCampaigns campaign = new GetCampaigns();
		CampaignSelectors cSelector = new CampaignSelectors();
		cSelector.setBudgetIDs(null);
		ArrayOfCampaignStatus status = new ArrayOfCampaignStatus();
		status.setCampaignStatus(new CampaignStatus[] { CampaignStatus.RUNNING });
		cSelector.setCampaignStatus(status);
		campaign.setCampaignSelector(cSelector);

		try {
			campaigns = servicesStub.getCampaigns(campaign, headerE);
		} catch (RemoteException e) {
			logger.error("Error in caling campaign endpoint " + e);
		}

		return campaigns;
	}

	private ApiHeaderE getAuthenticationHeader(CriteoAdvertiserAPIv201305Stub servicesStub) {
		ClientLogin loginreq = new ClientLogin();
		loginreq.setUsername(username);
		loginreq.setPassword(password);
		loginreq.setSource(source);

		ClientLoginResponse response = null;
		try {
			response = servicesStub.clientLogin(loginreq);
		} catch (RemoteException e1) {
			logger.error("Error in client login " + e1);
		}

		ApiHeaderE headerE = new ApiHeaderE();
		ApiHeader header = new ApiHeader();
		header.setAppToken(appToken);
		header.setAuthToken(response.getClientLoginResult());

		headerE.setApiHeader(header);
		return headerE;
	}

}
