
/**
 * CriteoAdvertiserAPIv201305CallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package com.metrics.criteo.service;

    /**
     *  CriteoAdvertiserAPIv201305CallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class CriteoAdvertiserAPIv201305CallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public CriteoAdvertiserAPIv201305CallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public CriteoAdvertiserAPIv201305CallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getCatalogsNames method
            * override this method for handling normal response from getCatalogsNames operation
            */
           public void receiveResultgetCatalogsNames(
                    com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.GetCatalogsNamesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCatalogsNames operation
           */
            public void receiveErrorgetCatalogsNames(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBudgets method
            * override this method for handling normal response from getBudgets operation
            */
           public void receiveResultgetBudgets(
                    com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.GetBudgetsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBudgets operation
           */
            public void receiveErrorgetBudgets(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAccount method
            * override this method for handling normal response from getAccount operation
            */
           public void receiveResultgetAccount(
                    com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.GetAccountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAccount operation
           */
            public void receiveErrorgetAccount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCategories method
            * override this method for handling normal response from getCategories operation
            */
           public void receiveResultgetCategories(
                    com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.GetCategoriesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCategories operation
           */
            public void receiveErrorgetCategories(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for partnerLogin method
            * override this method for handling normal response from partnerLogin operation
            */
           public void receiveResultpartnerLogin(
                    com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.PartnerLoginResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from partnerLogin operation
           */
            public void receiveErrorpartnerLogin(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for mutateCategories method
            * override this method for handling normal response from mutateCategories operation
            */
           public void receiveResultmutateCategories(
                    com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.MutateCategoriesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from mutateCategories operation
           */
            public void receiveErrormutateCategories(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for scheduleReportJob method
            * override this method for handling normal response from scheduleReportJob operation
            */
           public void receiveResultscheduleReportJob(
                    com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.ScheduleReportJobResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from scheduleReportJob operation
           */
            public void receiveErrorscheduleReportJob(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getReportDownloadUrl method
            * override this method for handling normal response from getReportDownloadUrl operation
            */
           public void receiveResultgetReportDownloadUrl(
                    com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.GetReportDownloadUrlResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getReportDownloadUrl operation
           */
            public void receiveErrorgetReportDownloadUrl(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getJobStatus method
            * override this method for handling normal response from getJobStatus operation
            */
           public void receiveResultgetJobStatus(
                    com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.GetJobStatusResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getJobStatus operation
           */
            public void receiveErrorgetJobStatus(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for scheduleTransactionReportJob method
            * override this method for handling normal response from scheduleTransactionReportJob operation
            */
           public void receiveResultscheduleTransactionReportJob(
                    com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.ScheduleTransactionReportJobResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from scheduleTransactionReportJob operation
           */
            public void receiveErrorscheduleTransactionReportJob(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getStatisticsLastUpdate method
            * override this method for handling normal response from getStatisticsLastUpdate operation
            */
           public void receiveResultgetStatisticsLastUpdate(
                    com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.GetStatisticsLastUpdateResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getStatisticsLastUpdate operation
           */
            public void receiveErrorgetStatisticsLastUpdate(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for mutateCampaigns method
            * override this method for handling normal response from mutateCampaigns operation
            */
           public void receiveResultmutateCampaigns(
                    com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.MutateCampaignsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from mutateCampaigns operation
           */
            public void receiveErrormutateCampaigns(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for clientLogin method
            * override this method for handling normal response from clientLogin operation
            */
           public void receiveResultclientLogin(
                    com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.ClientLoginResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from clientLogin operation
           */
            public void receiveErrorclientLogin(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCampaigns method
            * override this method for handling normal response from getCampaigns operation
            */
           public void receiveResultgetCampaigns(
                    com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.GetCampaignsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCampaigns operation
           */
            public void receiveErrorgetCampaigns(java.lang.Exception e) {
            }
                


    }
    