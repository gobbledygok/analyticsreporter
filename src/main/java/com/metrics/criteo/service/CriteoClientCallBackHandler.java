package com.metrics.criteo.service;

import org.apache.log4j.Logger;

import com.metrics.criteo.service.CriteoAdvertiserAPIv201305Stub.GetReportDownloadUrlResponse;

public class CriteoClientCallBackHandler extends CriteoAdvertiserAPIv201305CallbackHandler {

	private static final Logger logger = Logger.getLogger(CriteoClientCallBackHandler.class);

	@Override
	public Object getClientData() {
		return super.getClientData();
	}

	@Override
	public void receiveResultgetReportDownloadUrl(GetReportDownloadUrlResponse result_p) {
		super.receiveResultgetReportDownloadUrl(result_p);
	}

	@Override
	public void receiveErrorgetReportDownloadUrl(Exception ex_p) {
		super.receiveErrorgetReportDownloadUrl(ex_p);
	}

}
