package com.metrics.google.service;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleBrowserClientRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.plus.Plus;
import com.google.api.services.plus.Plus.Builder;

@Controller
public class GoogleApiController {

	private String clientId = "206521132078-pvdr8fo0d9iquuj7am7feu8retj301r6.apps.googleusercontent.com";
	private String clientSecret = "7sJw3lFN4TGfCYkoIrl9qTHN ";

	private String serviceAccountsKey = "462fe888fc8d1ac0a9afeb963d7a265d1d643002";
	private String serviceAccountName = "gobbledygook";

	private static final Logger logger = Logger.getLogger(GoogleApiController.class);

	@RequestMapping("/callBack")
	public String callBack() {

		return "";
	}

	@RequestMapping("/authenticate")
	public String authenticate() {
		String accessToken = "";
		try {
			GoogleCredential credential = new GoogleCredential().setAccessToken("7sJw3lFN4TGfCYkoIrl9qTHN");
			JacksonFactory jackson = new JacksonFactory();
			Plus plus = new Builder(new NetHttpTransport(), jackson, credential).setApplicationName("Web client")
					.build();
			logger.info("");
		} catch (Exception e) {
			logger.info("");
		}
		try {
			GoogleCredential credential = new GoogleCredential()
					.setAccessToken("206521132078-pvdr8fo0d9iquuj7am7feu8retj301r6.apps.googleusercontent.com");
			JacksonFactory jackson = new JacksonFactory();
			Plus plus = new Builder(new NetHttpTransport(), jackson, credential)
					.setApplicationName("Google-PlusSample/1.0").build();
			logger.info("");
		} catch (Exception e) {
			logger.info("");
		}

		return accessToken;

	}

	private AuthorizationCodeFlow flow;

	@RequestMapping("/authenticateToken")
	public void doGetLoginAccessURL(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String url = flow.newAuthorizationUrl().setState("xyz").setRedirectUri("https://client.example.com/rd").build();
		response.sendRedirect(url);
	}

	// public static Urlshortener newUrlshortener() {
	// AppIdentityCredential credential = new
	// AppIdentityCredential(Arrays.asList(UrlshortenerScopes.URLSHORTENER));
	// return new Urlshortener.Builder(new UrlFetchTransport(),
	// JacksonFactory.getDefaultInstance(), credential)
	// .build();
	// }

	@RequestMapping("/uploadReport")
	public @ResponseBody String uploadReport() {
		File file = new File("data.csv");
		// try {
		// // InputStreamContent mediaContent = new
		// // InputStreamContent("application/octet-stream",
		// // new FileInputStream(file));
		// // mediaContent.setLength(file.length());
		// } catch (FileNotFoundException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		// try {
		// analytics.management().uploads().uploadData("123456", "UA-123456-1",
		// "122333444455555", mediaContent)
		// .execute();
		// } catch (GoogleJsonResponseException e) {
		// System.err.println(
		// "There was a service error: " + e.getDetails().getCode() + " : " +
		// e.getDetails().getMessage());
		// }

		return "";

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		String url = new GoogleBrowserClientRequestUrl("812741506391.apps.googleusercontent.com",
				"https://oauth2.example.com/oauthcallback",
				Arrays.asList("https://www.googleapis.com/auth/userinfo.email",
						"https://www.googleapis.com/auth/userinfo.profile")).setState("/profile").build();
		try {
			response.sendRedirect(url);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static HttpResponse executeGet(HttpTransport transport, JsonFactory jsonFactory, String accessToken,
			GenericUrl url) throws IOException {
		Credential credential = new Credential(BearerToken.authorizationHeaderAccessMethod())
				.setAccessToken(accessToken);
		HttpRequestFactory requestFactory = transport.createRequestFactory(credential);
		return requestFactory.buildGetRequest(url).execute();
	}

}
